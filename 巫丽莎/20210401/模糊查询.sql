create database Student

create table stuinfo(
stuNO  nvarchar(50) not null,
stuName nvarchar(50) not null,
stuAge int not null,
stuAddress nvarchar(200) not null,
stuSeat int identity(1,1),
stuSex tinyint not null ,
);

insert into stuinfo(stuNO,stuName ,stuAge,stuAddress,stuSex)
		 values('s2501','张秋利',20,'美国硅谷',1)
		 ,('s2502','李斯文',18,'湖北武汉',0)
		 ,('s2503','马文才',22,'湖南长沙',1)
		 ,('s2504','欧阳俊雄',21,'湖北武汉',0)
		 ,('s2505','梅超风',20,'湖北武汉',1)
		 ,('s2506','陈旋风',19,'美国硅谷',1)
		 ,('s2507','陈风',20,'美国硅谷',0);

select * from stuinfo

--查询stuinfo中的姓名，年龄和地址三列信息
select stuName 姓名,stuAge 年龄,stuAddress 地址  from stuinfo

--.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select  stuName+'@'+stuAddress [姓名@地址] from stuinfo

--------------------------------------------------------------------------
--模糊查询  select * from 表名 where 字段 like '%查找的词%'
----模糊查询like
--常用的匹配符有：
-- %：匹配0或多个任意字符(最常用)
--  _:匹配任意一个字符
-- []:匹配指定范围内或是指定集合中的任意一个字符
-- [^]：匹配不是指定范围内或不是指定集合中的任意一个字符
---------------------------------------------------------------------------

--查询姓是欧阳的学生
select * from stuinfo where stuName like '欧阳%'

--查询地址是桂林市的学生
select * from stuinfo where stuAddress like '%桂林市%'

--查询姓李的学生，并且是三个字
select * from stuinfo where stuName like '李__'







