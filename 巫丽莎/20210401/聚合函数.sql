use Student;
go

--where 条件表达式

--查询软件一班学生信息
select * from Student where classid=1


--模糊查询like
--常用的匹配符有：
--- %：匹配0或多个任意字符(最常用)
--  _:匹配任意一个字符
-- []:匹配指定范围内或是指定集合中的任意一个字符
-- [^]：匹配不是指定范围内或不是指定集合中的任意一个字符

--示例1，查询地址是桂林市的学生：
select * from Student where studentaddress like '%桂林市%'

--查询姓名以刘开头的学生信息
select * from StuInfo where StuName like '刘%'


--查询姓名以'翔'为结尾的学生信息
select * from StuInfo where StuName like '%翔'

--查询姓名以刘开头姓名是三个字的学生信息
select * from StuInfo where StuName like '刘__'


--查询学生信息中手机号码为“13245678121”
select * from StuInfo where StuPhone ='13245678121'

--查询学生信息中手机号码为“1324567812_”
select * from StuInfo where StuPhone  like '1324567812_'

--查询学生信息中手机号码为“1324567812_”,最后一位范围是[1-6]
select * from StuInfo where StuPhone  like '1324567812[1-6]'

--查询学生信息中手机号码为“1324567812_”,最后一位范围是[1,3,5]
select * from StuInfo where StuPhone  like '1324567812[1,3,5]'

--查询学生信息中手机号码为“1324567812_”,最后一位范围不在[1,3,5]内
select * from StuInfo where StuPhone   like '1324567812[^135]'

-------------------------------------------------------------------------------------
--聚合函数
-- count(*|0|1):统计结果集的记录数;count(字段名)统计结果集中该字段非空的记录数
-- sum(字段名)：总数
-- avg（字段名）：平均数
-- max（字段名）：最大值
-- min（字段名）：最小值
--------------------------------------------------------------------------------------

--查询 软件一班 有几个学生
select count(*) 软件一班学生总数  from Student where ClassId=1

--示例2，查询 软件一班 有几门课程：
select count(*) 软件一班课程数 from ClassCourse where ClassId=1
select count(CourseId) 软件一班课程数 from ClassCourse where ClassId=1

--示例3，查询 刘正(学生编号为1) 的总分：
select sum(Score) 刘正的总分 from Score where StudentId=1

--distinct:消除重复
select count(distinct StudentId) 参加考试的人数 from Student

--查询所有学生的 数据高级应用 的平均分
select avg(Score) 数据高级应用的平均分 from Score where CourseId=3





