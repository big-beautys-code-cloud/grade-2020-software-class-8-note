---------------------------------------------------------------------------------------------
--分组查询：将结果集根据分组字段进行分组，通常与聚合函数一起使用实现分组统计查询
--group by 分组字段
--having 包含聚合函数的条件表达式    对统计分析后的数据进行条件过滤,条件表达式只能是聚合函数或是分组字段
--where  条件表达式  对数据源进行过滤，表达式中的字段是表中原有的字段
----------------------------------------------------------------------------------------------

--查询 每一个学生 的总分
select StudentId, sum(Score) 总分数 from Score 
group by StudentId

--查询 每一个学生 的平均分
select StudentId, avg(Score) 平均分 from Score
group by StudentId

--查询 每一个学生的平均分，并且按照平均分高低排(分组查询+排序)
select StudentId, avg(Score) 平均分 from Score
group by StudentId
order by avg(Score) desc

--查询学生信息表（stuInfo）中前3行记录 
select top 3  * from stuInfo

--查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 stuName 姓名, stuSeat 座位号 from stuInfo

--查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent * from stuInfo

--将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from stuinfo where stuAddress like '湖北武汉%'and stuAge=20

--将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列（用两种方法实现）
select * from Stuexam where labExam between 60 and 80 order by labExam desc
select * from Stuexam where  labExam > =60 and labExam <=80 order by labExam desc

--查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现）
select * from stuinfo where stuAddress like '湖北武汉%' or stuAddress like '湖南长沙%'
select * from stuinfo where stuAddress in ( '湖北武汉','湖南长沙')

--查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列（用两种方法实现）
select * from Stuexam where writtenExam not in (70,90) order by writtenExam asc
select * from Stuexam  where  writtenExam<70 and writtenExam >90 order by writtenExam asc

--查询年龄没有写的学生所有信息
select * from Student where StudentBirth is null

--查询年龄写了的学生所有信息
select * from Student where StudentBirth  is not null

--查询姓张的学生信息(我的表格没有张姓)
select * from Student where StudentName like'张%'

--查询学生地址中有‘湖’字的信息
select * from Student where StudentAddress like'%湖%'

--查询姓张但名为一个字的学生信息（没有张姓）
select * from Student where StudentName like'张_'

--查询姓名中第三个字为‘天’的学生的信息，‘天’后面有多少个字不限制
select * from Student where StudentName like'__天%'

--按学生的年龄降序显示所有学生信息
select * from Student order by StudentBirth asc

--按学生的年龄降序和座位号升序来显示所有学生的信息
select * from Student order by StudentBirth asc , StudentId desc

--显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 *from Stuexam order by writtenExam desc

--显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 *from Stuexam order by writtenExam asc