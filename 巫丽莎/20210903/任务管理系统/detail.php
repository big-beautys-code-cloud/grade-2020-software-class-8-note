<?php
$taskId=$_GET['task_id']??'';
$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "select * from Task where TaskId={$taskId}";
$result = $db->query($sql);
$taskInfo = $result->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>任务详情</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <a href="list.php">返回任务列表</a>
    <form>
        <table class="update">
            <caption>
                <h3>任务详情</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td><?php
                    echo $taskInfo['TaskName'];
                    ?>
                </td>
            </tr>
            <tr>
                <td>任务状态：</td>
                <td><?php
                    if ($taskInfo['TaskStatus']==1){
                        echo "新创建";
                    }else if ($taskInfo['TaskStatus']==2){
                        echo "进行中";
                    }else if ($taskInfo['TaskStatus']==3){
                        echo "已完成";
                    }
                    ?></td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea cols="60" rows="15"
                              readonly="readonly"><?php
                        echo $taskInfo['TaskContent'];
                        ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a class="update_ing" href="javascript:if(confirm('确定要标记为进行中吗？'))window.location='update_ing.php?task_id=<?php echo $taskInfo['TaskId']?>'">标记为进行中</a>
                </td>
            </tr>

        </table>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>
