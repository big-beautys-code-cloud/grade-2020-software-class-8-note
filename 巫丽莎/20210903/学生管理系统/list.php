<?php
/**
 * 学生列表页面
 */

// 连接数据库，查询出所有的班级信息
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Class order by ClassId desc';
$result = $db->query($sql);
$classList = $result->fetchAll(PDO::FETCH_ASSOC);
//var_dump($classList);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>
<body>
<div id="container">
    <a id="add" href="add.php">增加</a>
    <table class="list">
        <tr>
            <th>班级id</th>
            <th>班级名称</th>
            <th>操作</th>
        </tr>
        <?php foreach ($classList as $key => $value): ?>
            <tr>
                <td><?php echo $value['ClassId']; ?></td>
                <td><?php echo $value['ClassName']; ?></td>
                <td>
                    <a href="update.php?class_id=<?php echo $value['ClassId']; ?>">修改</a>
                    <a href="delete.php?class_id=<?php echo $value['ClassId']; ?>">删除</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
</body>
</html>

