<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/24
 * Time: 15:35
 */
// 取到班级id
//var_dump($_GET);
$classId = $_GET['class_id'] ?? '';

// 查询修改的班级信息
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");

$sql = "select * from Class where ClassId='{$classId}'";
$result = $db->query($sql);
$classInfo = $result->fetch(PDO::FETCH_ASSOC);
//var_dump($classInfo);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="update_save.php">
        <table class="update">
            <caption>
                <h3>修改班级信息</h3>
            </caption>
            <tr>
                <td>班级id：</td>
                <td><input type="text" name="class_id" value="<?php echo $classInfo['ClassId']; ?>" readonly="readonly" /></td>
            </tr>
            <tr>
                <td>班级名称：</td>
                <td><input type="text" name="class_name" value="<?php echo $classInfo['ClassName']; ?>"/></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

