

--distinct:消除重复
select distinct StudentId from Score;

---------------------------------------------
--聚合函数：将多行数据进行统计分析获得一个统cou'ne计值
--count(字段)：统计字段值的个数，不包含控制；count(*)统计结果集的记录行数
--sum(字段)：计算字段的累加和
--avg(字段)：计算字段的平均值
---------------------------------------------
--查询参加考试的人数
select count(distinct StudentId) 参考人数 from score ;

--查询成绩表中所有学生的总成绩，平均分，最大值，最小值;
select sum(Score) 总成绩,avg(Score) 平均分,max(Score) 最大值,min(Score) 最小值 from Score

-------------
--分组查询：将结果集根据分组字段进行分组，通常与聚合函数一起使用实现分组统计查询
--group by 分组字段
--having 包含聚合函数的条件表达式    对统计分析后的数据进行条件过滤,条件表达式只能是聚合函数或是分组字段
--where  条件表达式  对数据源进行过滤，表达式中的字段是表中原有的字段
------------
--1.查询每个同学的总成绩（根据学号进行分组统计）
select StudentId,sum(Score) 总成绩 from Score
group by  StudentId

--查询每门课程的平均成绩（根据课程编号进行分组统计）
select CourseId,avg(Score) 平均分 from Score
group by CourseId

--查询每个同学的平均分，按平均分由高到低进行排序(分组查询+排序)
select StudentId,avg(Score) as 平均分 from Score 
group by StudentId
order by avg(Score) desc

--查询每个同学的平均分，并且显示平均分大于70的成绩信息
select StudentId,avg(Score) as 平均分 from Score 
group by StudentId
having  avg(Score)>70


--查询学号1-5的学生的平均分信息
select StudentId,avg(Score) as 平均分 from Score 
group by StudentId
having StudentId between 1 and 5

--查询学号1-5学生课程编号1-3的平均分信息
select StudentId,avg(Score) 平均分 from Score 
where CourseId between 1 and 3
group by StudentId
having StudentId between 1 and 5


--查询每个学生课程编号1-3的平均分信息,平均分大于70
select StudentId,avg(Score) 平均分 from Score 
--where
group by StudentId
having avg(Score)>70 

--查询每个学生每门课程的平均分
select StudentId,CourseId,avg(Score) 平均分 from Score
group by StudentId,CourseId



----------------------------------------------------------
--总结：
--select [top n [percent] | distinct] <列名1,列名2,......>   查询结果的列名列表，top 截取结果集的前几条记录;distinct消除重复
--from <表名>			从哪些表取数据，数据源
--[where  条件表达式]  对数据源进行过滤，表达式中的字段是表中原有的字段
--[group by <分组字段1，分组字段2，.......>]  将表的数据根据分组字段进行分组统计
--[having 包含聚合函数的条件表达式]    对分组统计后的数据进行条件过滤,条件表达式只能是聚合函数或是分组字段
--[order by <排序字段1 [asc|desc],排序字段2 [asc|desc],.....>] 将结果集按照排序字段指定的排序方式进行排序
-----------------------------------------------------------
