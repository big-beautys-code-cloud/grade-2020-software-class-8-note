
--创建库
create database Bank;

--创建表
create table AccountInfo(
AccountId int identity(1,1) not null,--自增，不允许为空
AccountCode varchar(20) not null,
AccountPhone varchar(20),
RralName varchar(20) not null,
);
create table BankCard(
CardNo varchar(30)  primary key ,--主键
AccountId int not null,
CardPwd	varchar(30) not null,
CardBalance	money not null,
CardState	tinyint not null,
CardTime	varchar(30) not null,
);
create table  CardExchange(
ExchangeId	int identity(1,1) primary key,
CardNo	varchar(30) not null,
MoneyInBank	money not null,
MoneyOutBank	money not null,
ExchangeTime	smalldatetime not null,
);
--添加字段alter table <表名> add <新字段名><数据类型>[约束条件];
alter table AccountInfo add OpenTime smalldatetime not null

--修改数据类型alter table<表名> alter column <列名/字段名> <数据类型>
alter table BankCard alter column CardTime	smalldatetime

--主键约束（添加主键）alter table 表名 add constraint 约束名 primary key (字段);
alter table AccountInfo add constraint PK_AccountInfo_AccountId primary key (AccountId)

--唯一约束alter table 表名 add constraint 约束名 unique(字段);
alter table AccountInfo add constraint UNQ_AccountInfo_AccountCode unique (AccountCode)

--默认值约束(添加默认值)alter table 表名 add constraint 约束名 default(默认值) for 字段;
alter table AccountInfo add constraint DF_AccountInfo_OpenTime default(getdate()) for OpenTime 

--添加非空alter table 表名 alter column 列名 数据类型 mot null
alter table AccountInfo alter column AccountPhone varchar not null

--设置默认值alter table 表名 add constraint 约束名 default(默认值) for 字段;
alter table BankCard add constraint DF_BankCard_CardBalance default(0.00) for CardBalance 

--设置默认值
alter table BankCard add constraint DF_BankCard_CardState default(1) for CardState

--设置外键alter table 表名 add constraint 约束名 foreign key (字段名) references 被关联表名(字段名);
alter table CardExchange add constraint FK_CardExchange_CardNo  foreign key (CardNo ) references BankCard(CardNo)

--设置check约束alter table 表名 add constraint 约束名 check(约束条件);
alter table CardExchange add constraint CK_CardExchange_MoneyInBank  check(MoneyInBank>0)
 
 alter table CardExchange add constraint CK_CardExchange_MoneyOutBank  check(MoneyOutBank>0)