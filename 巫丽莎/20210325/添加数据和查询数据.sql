create database Student
--创建班级表
create table Class(
 ClassId int not null identity(1,1),
 ClassName nvarchar(50) not null
);

--插入数据
insert into Class (ClassName) values ('软件一班');
insert into Class (ClassName) values ('软件二班');
insert into Class (ClassName) values ('计算机应用技术班');

--查询班级表的所有数据
select * from Class 

--创建学生表
create table Student (
    StudentId int not null identity(1, 1),
    StudentName nvarchar(50),
    StudentSex tinyint not null ,
    StudentBirth date,
    StudentAddress nvarchar(255) not null,
    ClassId int
);

--查询学生表的所以数据
select * from Student

--批量插入数据（一班）
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('刘正',1,'2000-01-01','广西省桂林市七星区空明西路10号鸾东小区', 1)
  ,('黄贵',1,'2001-03-20','江西省南昌市青山湖区艾溪湖南路南150米广阳小区', 1)
  ,('陈美',2,'2000-07-08','福建省龙岩市新罗区曹溪街道万达小区', 1);

  --（二班）
  insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('江文',1,'2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光', 2)
        , ('钟琪',2,'2001-03-21','湖南省长沙市雨花区红花坡社区', 2);

--(三班)
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('曾小林',1,'1999-12-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光', 3)
        ,('欧阳天天',2,'2000-04-05','湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府', 3)
        ,('徐长卿',1,'2001-01-30','江苏省苏州市苏州工业园区独墅湖社区', 3)
        ,('李逍遥',1,'1999-11-11','广东省广州市白云区金沙洲岛御洲三街恒大绿洲', 3);


--课程表
create table Course(
CourseId int identity(1,1),
CourseName nvarchar(50),
CourseCredit int,
);
insert into Course(CourseName, CourseCredit) values ('数据库高级应用', 3)
    ,('javascript编程基础', 3)
    ,('web前端程序设计基础', 4)
    ,('动态网页设计.net基础', 6);

-- 班级课程表
create table  ClassCourse(
ClassId int,
 CourseId int,
);
-- 一班
insert into ClassCourse (ClassId, CourseId) values (1, 1)
    ,(1, 2)
    ,(1, 3)
    ,(1, 4);

-- 二班
insert into ClassCourse (ClassId, CourseId) values (2, 1)
    ,(2, 2)
    ,(2, 3)
    ,(2, 4);

-- 三班
insert into Course(CourseName, CourseCredit) values ('动态网页设计php基础', 6);
insert into ClassCourse (ClassId, CourseId) values (3, 1)
    ,(3, 2)
    ,(3, 3)
    ,(3, 5);

--成绩表
create table Score(
StudentId int ,
CourseId int,
Score int,
);
insert into Score (StudentId, CourseId, Score) values (1, 1, 80)
    ,(1, 2, 78)
    ,(1, 3, 65)
    ,(1, 4, 90)
    ,(2, 1, 60)
    ,(2, 2, 77)
    ,(2, 3, 68)
    ,(2, 4, 88)
    ,(3, 1, 88)
    ,(3, 2, 45)
    ,(3, 3, 66)
    ,(3, 4, 75);
insert into Score (StudentId, CourseId, Score) values (4, 1, 56)
    ,(4, 2, 80)
    ,(4, 3, 75)
    ,(4, 4, 66)
    ,(5, 1, 88)
    ,(5, 2, 79)
    ,(5, 3, 72)
    ,(5, 4, 85);
insert into Score (StudentId, CourseId, Score) values (6, 1, 68)
    ,(6, 2, 88)
    ,(6, 3, 73)
    ,(6, 5, 63)
    ,(7, 1, 84)
    ,(7, 2, 90)
    ,(7, 3, 92)
    ,(7, 5, 78)
    ,(8, 1, 58)
    ,(8, 2, 59)
    ,(8, 3, 65)
    ,(8, 5, 75)
    ,(9, 1, 48)
    ,(9, 2, 67)
    ,(9, 3, 71)
    ,(9, 5, 56)
    ,(9, 5, 56);


--查询所有的分数 Score 信息
select * from Score

--查询所有的班级课程 ClassCourse 信息
select * from ClassCourse

--查询所有的课程 Course 信息
select *from Course

--查询所有的学生Student
select * from Student

--查询所有的班级Class信息
select * from Class 

--查询所有的课程 Course 信息，并且按照学分从大到小排列
--select * from 表名 order by 字段 desc|asc; （降序/升序）
select * from Course order by CourseCredit desc;

--查询所有的分数 Score 信息，并且按照分数从小到大排列
select * from Score order by  Score asc;

--查询所有的学生 Student 信息，并且按照年龄从小到大排列
select * from Student order by  StudentBirth asc;

--指定别名 select 列名1 as 别名1, 列名2  as 别名1, 列名3 as 别名3, ... from 表名
select StudentName as 学生姓名, StudentBirth as 学生生日 from Student;
select StudentName 学生姓名, StudentBirth 学生生日 from Student;

--查询若干条数据 select top n * from 表名
select top 3 * from Student

--查询指定列 select 列名1,列名2,列名3,... from 表名
select StudentName, StudentBirth from Student

--查询百分之五十的数据/若干数据
select top 50 percent * from Student

--显示一个结果为 姓名：地址 的表格
select StudentName+':'+StudentAddress [姓名：地址] from Student

--条件筛选 select * from 表名 where 条件
--  =	等于
--  <>	不等于
--  >	大于
--  <	小于
--  >=	大于等于
--  <=	小于等于
--  between	在某个范围内
--  is null	为空
--  is not null	不非空
--  in	包含在内
--  not in	不包含在内
--  like	搜索某种模式

--查询软件一班所以学生
select * from Student where ClassId = 1

--查询不是软件一班所有的学生
select *from Student where ClassId<>1

--查询2000年出生的所有学生
select * from Student where StudentBirth >= '2000-01-01' and StudentBirth < '2001-01-01';

--查询2000年出生的所有学生 between
select * from Student where StudentBirth between '2000-01-01' and '2000-12-31';

--查询软件一班和计算机应用技术班所有的学生
select * from Student where ClassId = 1 or ClassId = 3;

-- 查询所有的女生，显示学生id，姓名、性别、生日、住址
select * from  Student where StudentSex=2

