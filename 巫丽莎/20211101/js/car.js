$(function() {
    // 点击全选按钮，所有商品的复选框状态会变更
    $('.checkall').click(function() {
        $('.j-checkbox').prop('checked', $(this).prop('checked'));
        $('.checkall').prop('checked', $(this).prop('checked'));
    });

    // 商品复选框选中状态切换，全选状态也会切换
    $('.j-checkbox').click(function() {
        // 判断选中的数量和所有复选框的数量是否一致
        let len1 = $('.j-checkbox').length;
        let len2 = $('.j-checkbox:checked').length;
        if (len1 === len2) {
            $('.checkall').prop('checked', true);
        } else {
            $('.checkall').prop('checked', false);
        }
    });

    // 点击+号，增加数量
    $('.increment').click(function() {
        
        let num = $(this).siblings('.itxt').val();
        num++;
        $(this).siblings('.itxt').val(num);

        // 修改小计的值
        let price = $(this).closest('.p-num').siblings('.p-price').text();
        let price2 = price.substr(1, price.length - 1);
        let sum = num * Number(price2);
        $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
        getSum();
    });

    $('.decrement').click(function() {
        let num = $(this).siblings('.itxt').val();
        if (num > 1) {
            num--;
            $(this).siblings('.itxt').val(num);

            let price = $(this).closest('.p-num').siblings('.p-price').text();
            let price2 = price.substr(1, price.length - 1);
            let sum = num * Number(price2);
            $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
            getSum();
        }
    });
   
    

    function getSum(){        
        var count = 0;
        var money = 0;

        $(".itxt").each(function(index) {
            if ($(".j-checkbox").eq(index).prop("checked") == true) {
            count += parseInt($(".itxt").eq(index).val());
            money += parseInt($(".p-sum").eq(index).text().substr(1));
            }
        })
            $(".amount-sum em").html(count);
            $('.price-sum em').text('￥' + money.toFixed(2));
    }
    

})

    

