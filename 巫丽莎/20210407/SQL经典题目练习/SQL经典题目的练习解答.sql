--1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数
select * from StudentInfo
inner join 
(select a.StudentId,a.CourseName 数学,a.Score 数学成绩,b.CourseName 语文,b.Score 语文成绩  from
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='数学') a
left join
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='语文')  b
on a.StudentId=b.StudentId
where a.Score>b.Score
) score 
on StudentInfo.id=score.StudentId

--1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
select * from StudentInfo
inner join 
(select a.StudentId,a.CourseName 数学,a.Score 数学成绩,b.CourseName 语文,b.Score 语文成绩  from
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='数学') a
inner join
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='语文')  b
on a.StudentId=b.StudentId
) score
on StudentInfo.id=score.StudentId



--1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
select * from StudentInfo
inner join 
(select a.StudentId,a.CourseName 数学,a.Score 数学成绩,b.CourseName 语文,b.Score 语文成绩  from
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='数学') a
left join
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='语文')  b
on a.StudentId=b.StudentId
) score
on StudentInfo.id=score.StudentId

--1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
select * from StudentInfo
inner join 
(select a.StudentId,a.CourseName 数学,a.Score 数学成绩,b.CourseName 语文,b.Score 语文成绩  from
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='数学') a
right join
(select StudentCourseScore.*,CourseName from StudentCourseScore,CourseInfo where StudentCourseScore.CourseId=CourseInfo.id and CourseName='语文')  b
on a.StudentId=b.StudentId
where a.Score is null
) score
on StudentInfo.id=score.StudentId

--2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
select b.StudentId,a.StudentName,avg(b.Score) 平均成绩 from StudentInfo a
inner join StudentCourseScore b
on a.id=b.StudentId
group by b.StudentId,a.StudentName

--3.查询在 成绩 表存在成绩的学生信息
select * from StudentInfo where id in (select distinct StudentId from StudentCourseScore)

--4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select StudentInfo.Id,StudentInfo.StudentName,选课总数,总成绩 from StudentInfo
left join (select StudentId,COUNT(*) 选课总数,sum(Score) 总成绩 from StudentCourseScore group by StudentCourseScore.StudentId) a
on StudentInfo.id=a.StudentId



--4.1 查有成绩的学生信息
select StudentInfo.* from StudentInfo
inner join StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.Id
where Score is not null

--5.查询「李」姓老师的数量
select count(*) ['李'姓老师的数量] from Teachers where TeacherName like '李%'

--6.查询学过「张三」老师授课的同学的信息
select StudentInfo.* from StudentInfo 
inner join  StudentCourseScore on StudentCourseScore.StudentId=StudentInfo.id
inner join CourseInfo on CourseInfo.id=StudentCourseScore.CourseId
inner join Teachers on Teachers.id=CourseInfo.TeacherId
where TeacherName='张三'

--7.查询没有学全所有课程的同学的信息
select * from StudentInfo where id not in(
select StudentId from StudentCourseScore group by StudentId
having count(*)=(select count(*) from CourseInfo )
)

--8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
--方案1：
select distinct  StudentInfo.* from StudentInfo
inner join StudentCourseScore on StudentInfo.id=StudentCourseScore.StudentId
where StudentCourseScore.CourseId in (select CourseId from StudentCourseScore where StudentId=1)
and StudentInfo.id!=1

--方案二：
select  StudentInfo.* from StudentInfo where id in(
select StudentId from StudentCourseScore where CourseId= any(select CourseId from StudentCourseScore where StudentId=1)
and StudentId!=1
)

--9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息

select * from StudentInfo where id in
(
--课程与'01'学生课程完全相同
select StudentId from
--课程编号与01选修的课程相同的成绩信息结果集
(select * from StudentCourseScore where CourseId in
(
	--学号为01的学生选修的课程编号
	select CourseId from StudentCourseScore where StudentId=1
)) b
group by StudentId --根据学号分组统计选修的课程数，课程数与01课程数相同
having count(*)=(select count(*) from StudentCourseScore where StudentId=1)
intersect
--选修课程数等于01编号选修的课程数
select StudentId from StudentCourseScore group by StudentId
having COUNT(*)=(select COUNT(*) from StudentCourseScore where StudentId=1)
)
and id!=1


--10.查询没学过"张三"老师讲授的任一门课程的学生姓名
select StudentName from StudentInfo where Id not in
(
	select StudentCourseScore.StudentId from StudentCourseScore 
	inner join CourseInfo on StudentCourseScore.CourseId=CourseInfo.id
	inner join Teachers on Teachers.Id=CourseInfo.TeacherId
	where Teachers.TeacherName='张三'
)

--11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

select StudentId 学号,StudentName 姓名,avg(Score) 平均分 from StudentInfo
inner join StudentCourseScore
on  StudentCourseScore.StudentId=StudentInfo.Id
where Score<60  
group by StudentId,StudentName having count(*)>=2

--12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息
select * from StudentInfo
inner join StudentCourseScore on StudentInfo.id= StudentCourseScore.StudentId
inner join CourseInfo on StudentCourseScore.CourseId=CourseInfo.Id
where CourseName='数学' and Score<60
order by Score

--13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select a.*,avgs 平均成绩 from StudentCourseScore a
inner join (select StudentId,avg(Score) avgs from StudentCourseScore group by StudentId) b
on a.StudentId=b.StudentId
order by b.avgs desc


--14.查询各科成绩最高分、最低分和平均分：
select CourseName 课程名称,max(Score) 最高分,min(Score) 最低分,avg(Score) 平均分 from StudentCourseScore
inner join CourseInfo on CourseInfo.id=StudentCourseScore.CourseId
group by CourseName


--15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
--/*
--	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

--	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

--*/	
	select CourseId 课程号,count(*) 选修人数,max(Score) 最高分,min(Score) 最低分,avg(Score) 平均分,
	round(sum(及格)/CONVERT(decimal(5,2),count(*)),2) 及格率,round(sum(中等)/CONVERT(decimal(5,2),count(*)),2) 中等率,round(sum(优良)/CONVERT(decimal(5,2),count(*)),2) 优良率,round(sum(优秀)/CONVERT(decimal(5,2),count(*)),2) 优秀率 from (
	select *,(case when Score>=60 then 1 else 0 end ) 及格,
	case when Score between 70 and 80 then 1 else 0 end 中等,
	case when Score between 80 and 90 then 1 else 0 end 优良,
	case when Score>=90 then 1 else 0 end 优秀
	from StudentCourseScore) as a
	group by CourseId
	
--15.1按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
select *,rank() over(partition by courseid order by score desc) 排名 from StudentCourseScore

--15.2 按各科成绩进行排序，并显示排名， Score 重复时合并名次
select *,dense_rank() over(partition by courseid order by score desc) 排名 from StudentCourseScore 

--16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select StudentId,sum(Score) 总分,rank() over(order by sum(score) desc) 排名 from StudentCourseScore group by StudentId

--16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
select StudentId,sum(Score) 总分,dense_rank() over(order by sum(score) desc) 排名 from StudentCourseScore group by StudentId
--17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
select CourseId,CourseName,sum([100-85]) '[100-85]人数',convert(decimal(5,2),sum([100-85])/count(StudentId)) '[100-85]百分比',sum([85-70]) '[85-70]人数',sum([85-70])/count(StudentId) '[85-70]百分比',sum([70-60]) '[70-60]人数',sum([70-60])/count(StudentId) '[70-60]百分比',sum([60-0])  '[60-0]人数',sum([60-0])/count(StudentId)  '[60-0]百分比'
from 
(select *,case when Score between 85 and 100 then 1 else 0 end [100-85],
		 case when Score between 70 and 85 then 1 else 0 end [85-70],
		 case when Score between 60 and 70 then 1 else 0 end [70-60],
		 case when Score between 0 and 60 then 1 else 0 end [60-0]	
from StudentCourseScore)  as a
inner join CourseInfo on CourseInfo.Id=a.CourseId
group by a.CourseId,CourseInfo.CourseName


--18.查询各科成绩前三名的记录
select * from
(select *,RANK() over(partition by courseid order by score) as 排名 from StudentCourseScore)  a
where a.排名<=3

--19.查询每门课程被选修的学生数
select CourseId,CourseName,count(Score) 选修学生数 from CourseInfo
left join StudentCourseScore on StudentCourseScore.CourseId=CourseInfo.Id
group by CourseId,CourseName
order by CourseId


--20.查询出只选修两门课程的学生学号和姓名
select * from StudentInfo where id in(
select StudentId from StudentCourseScore
group by StudentId
having count(Score)=2
)
--21.查询男生、女生人数
select Sex,COUNT(*) 人数 from StudentInfo
group by Sex

--22.查询名字中含有「风」字的学生信息
select * from StudentInfo where StudentName like '%风%'

--23.查询同名同姓学生名单，并统计同名人数
select StudentName,count(*) 同名人数 from StudentInfo 
group by StudentName
having count(*)>=2

--24.查询 1990 年出生的学生名单
select * from StudentInfo where Birthday like '1990%'

--25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
select CourseId,CourseName,AVG(Score) 平均成绩 from StudentCourseScore a
inner join CourseInfo on CourseInfo.Id=a.CourseId
group by a.CourseId,CourseName
order by AVG(Score) desc,CourseId


--26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select StudentId 学号,StudentName 姓名,avg(Score) from StudentInfo a
inner join StudentCourseScore b on b.StudentId=a.Id
group by StudentId,StudentName
having avg(Score)>=85

--27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select * from StudentInfo
inner join StudentCourseScore on StudentInfo.id=StudentCourseScore.StudentId
inner join CourseInfo on CourseInfo.id=StudentCourseScore.CourseId
where CourseName='数学' and Score<60
select * from StudentCourseScore where CourseId=2
select * from CourseInfo 

--28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select a.*,CourseName,Score from StudentInfo a
left join StudentCourseScore b on a.Id=b.StudentId
left join CourseInfo c on c.Id=b.CourseId

--29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
select StudentName,CourseName,Score from StudentInfo a
left join StudentCourseScore b on a.Id=b.StudentId
left join CourseInfo c on c.Id=b.CourseId
where Score>70

--30.查询不及格的课程
select distinct CourseName from CourseInfo
inner join StudentCourseScore on CourseInfo.Id=StudentCourseScore.CourseId

--31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
select * from StudentInfo
inner join StudentCourseScore on StudentInfo.id=StudentCourseScore.StudentId
where CourseId=1 and Score>=80

--32.求每门课程的学生人数
select CourseId,CourseName,count(*) 选修人数 from StudentCourseScore
inner join CourseInfo on CourseInfo.Id=StudentCourseScore.CourseId
group by CourseId,CourseName

--33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
select top 1 StudentInfo.*,Score from StudentInfo 
inner join StudentCourseScore on StudentInfo.Id=StudentCourseScore.StudentId
inner join CourseInfo on CourseInfo.Id=StudentCourseScore.CourseId
inner join Teachers on CourseInfo.TeacherId=Teachers.Id
where TeacherName='张三'
order by Score desc

--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
select  StudentInfo.*,CourseId,Score from StudentInfo 
inner join StudentCourseScore on StudentInfo.Id=StudentCourseScore.StudentId
inner join CourseInfo on CourseInfo.Id=StudentCourseScore.CourseId
inner join Teachers on CourseInfo.TeacherId=Teachers.Id
where TeacherName='张三' and Score in(
select max(Score) from StudentCourseScore 
inner join CourseInfo on CourseInfo.Id=StudentCourseScore.CourseId
inner join Teachers on CourseInfo.TeacherId=Teachers.Id
where TeacherName='张三'
)


--35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
select distinct a.StudentId 学生编号,a.CourseId 课程编号,a.Score from StudentCourseScore a 
inner join StudentCourseScore b on a.StudentId=b.StudentId
where a.Score=b.Score and a.CourseId!=b.CourseId

--36.查询每门功成绩最好的前两名
select * from
(select *,rank() over(partition by courseId order by score desc) [rank] from StudentCourseScore) a
where a.[rank]<=2


--37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。
select CourseId,count(StudentId) 选修人数 from StudentCourseScore
group by CourseId 
having count(StudentId)>5

--38.检索至少选修两门课程的学生学号
select StudentId from StudentCourseScore
group by StudentId
having count(Score)>=2

--39.查询选修了全部课程的学生信息
select StudentId from StudentCourseScore
group by StudentId
having count(Score)=(select COUNT(*) from CourseInfo)

--40.查询各学生的年龄，只按年份来算
select *,datediff(year,Birthday,GETDATE()) age from StudentInfo 

--41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
select *,
case 
	when dateadd(year,datediff(year,Birthday,GETDATE()),birthday)<getdate() then  DATEDIFF(YEAR,birthday,GETDATE())-1
	else  DATEDIFF(YEAR,birthday,GETDATE()) 
end	年龄	
from StudentInfo

--42.查询本周过生日的学生
--select * from StudentInfo where DATEPART(WEEK,Birthday)=DATEPART(WEEK,GETDATE())
declare @dt1 date
set @dt1=DATEADD(week,DATEDIFF(week,0,getdate()),0);
declare @dt2 date
set @dt2=DATEADD(week,DATEDIFF(week,0,getdate())+1,0)-1
select *,DATEADD(Year,DATEDIFF(Year,Birthday,@dt2),Birthday),DATEADD(Year,DATEDIFF(Year,Birthday,@dt1),Birthday) from StudentInfo
WHERE DATEADD(Year,DATEDIFF(Year,Birthday,@dt1),Birthday) between @dt1 and @dt2


--43.查询下周过生日的学生
declare @dt1 date
set @dt1=DATEADD(week,DATEDIFF(week,0,getdate())+1,0);
declare @dt2 date
set @dt2=DATEADD(week,DATEDIFF(week,0,getdate())+2,0)-1
select * from StudentInfo
WHERE DATEADD(Year,DATEDIFF(Year,Birthday,@dt1),Birthday) between @dt1 and @dt2

--44.查询本月过生日的学生
select * from StudentInfo where DATEPART(MONTH,Birthday)=DATEPART(MONTH,GETDATE())

--45.查询下月过生日的学生
select * from StudentInfo where DATEPART(MONTH,Birthday)=DATEPART(MONTH,GETDATE())+1