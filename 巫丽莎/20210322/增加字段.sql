create database Student;

create table Student(
StudentId	int	identity(1,1) not null primary key,
StudentName	nvarchar(50) not null,
StudentSex	tinyint	not null  default '3',
StudentBirth	date,
StudentAddress nvarchar(255) not null,
ClassId	int	not null,
);
select * from Student;
alter table Student add StudentIdentityCard varchar(20) not null  default '';