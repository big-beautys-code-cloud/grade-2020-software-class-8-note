<?php
//3. 使用正则表达式检测一个字符串是邮箱，并用php代码实现。
$email="1449827368@qq.com";
$result3=preg_match("/^([a-zA-Z0-9])+\@([a-zA-Z0-9])+(\.[a-zA-Z0-9])+/",$email);  // （\@）字符转义
if ($result3>0){
    echo "字符串符合格式";
}else{
    echo "字符串不符合格式";
}

echo "</br>";


//4. 使用正则表达式判断一个字符串是手机号，并用php代码实现。
$Phone="15159226535";
$result4=preg_match("/^1\d{10}$/",$Phone);
if ($result4>0){
    echo "字符串符合格式";
}else{
    echo "字符串不符合格式";
}