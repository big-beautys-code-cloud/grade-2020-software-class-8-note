<?php

//1. 使用正则表达式查询一个字符串是否全部由数字组成，并用php代码实现
$a="1234567";
$result=preg_match("/^\d{0,}$/",$a);   //^\d\d|+
if ($result>0){
    echo "字符串符合格式";
}else{
    echo "字符串不符合格式";
}

echo "<br/>";

//2. 使用正则表达式查询一个字符串是否全部由字母组成，并用php代码实现。
$b="asdfg";
$result1=preg_match("/^[a-zA-Z]+$/",$b);
if ($result1>0){
    echo "字符串符合格式";
}else{
    echo "字符串不符合格式";
}
