



---------------------------------------------------------------------------------------

--内连接（inner join）：将两张表的所有记录进行一对一的连接，只返回符合连接条件的记录

--select * from 表1 inner join 表2 on 表1.列 = 表2.列

---------------------------------------------------------------------------------------

--示例1，查询 所有学生信息 和 所在的班级信息（两表所有列）

select * from Student inner join Class on Student.ClassId=Class.ClassId


--示例2，查询 所有学生信息 和 所在的【班级名称(字段)】

select Student.*,ClassName from Student inner join Class on Student.ClassId=Class.ClassId 


--示例3，查询 李逍遥(编号id为9) 所在的班级，显示学生id、姓名、班级名称（连接查询 2表）：

select StudentId,StudentName,ClassName from Student inner join Class on Student.ClassId=Class.ClassId where StudentId=9


--示例4，查询 软件1班 学习的课程有哪几门，显示班级id、课程名称、课程学分（连接查询）：

select ClassId,CourseName,CourseCredit from Class inner join Course on Class.ClassId=Course.CourseId where ClassName='软件一班'


--示例5：查询学生的成绩信息：学号，姓名，课程编号，成绩字段

select Student.StudentId,StudentName,Course.CourseId,Score from Score 
inner join Student on Score.StudentId=Student.StudentId 
inner join Course on Course.CourseId=Score.CourseId


--示例6：查询学生的成绩信息：学号，姓名，课程名称，成绩字段

select Student.StudentId,StudentName,Course.CourseName,Score from Score 
inner join Student on Score.StudentId=Student.StudentId 
inner join Course on Course.CourseId=Score.CourseId


--示例7，查询 李逍遥 学习的课程有哪几门，显示学生id、姓名、课程名称、课程学分（连接查询）：

select StudentId,StudentName,Course.CourseName,CourseCredit from Student inner join ClassCourse on Student.ClassId=ClassCourse.ClassId 
inner join Course on ClassCourse.CourseId=Course.CourseId where StudentName='李逍遥'





