




---------------------------------------------------------------

--子查询:将查询语句作为另一个查询语句的一部分，就称为子查询

--select * from 表名 where 列 in (select 指定列 from 表名 ...)
--列和指定是一样的或者是聚合函数，后面的表名是条件的
---------------------------------------------------------------

--示例1，查询钟琪和曾小林的考试成绩
--查出他们的学生id
select StudentId from Student where StudentName='钟琪' or StudentName='曾小林'
--将查询出来的学号作为成绩表的筛选条件值
select * from Score where StudentId in (select StudentId from Student where StudentName='钟琪' or StudentName='曾小林')

select * from Score where StudentId in (select StudentId from Student where StudentName in ('钟琪' , '曾小林'))

--示例2，查询不为软件1班和软件2班的学生信息
select * from Student where ClassId not in (select ClassId from Class where ClassName='软件一班' or ClassName='软件二班')

select * from Student where ClassId not in(select ClassId from Class where ClassName  in('软件一班' , '软件二班'))

--示例3：显示成绩第一名的成绩信息
select * from Score where Score=(select max(Score) from Score)


--示例4：显示成绩第一名的学生信息
select * from Student where StudentId in (select StudentId from Score where Score=(select max(Score) from Score))




