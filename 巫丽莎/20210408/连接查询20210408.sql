

---------------------------------------------------------------------------------------

--内连接（inner join）：将两张表的所有记录进行一对一的连接，只返回符合连接条件的记录

--select * from 表1 inner join 表2 on 表1.列 = 表2.列

---------------------------------------------------------------------------------------

--示例1，查询 所有学生信息 和 所在的班级信息（两表所有列）
--连接学生信息和班级信息两张表，连接条件是两表的班级编号字段
select * from Student 
inner join Class on Class.ClassId = Student.ClassId


--示例2，查询 所有学生信息 和 所在的班级名称
select Student.*,ClassName from Student 
inner join Class on Class.ClassId = Student.ClassId


--示例3，查询 李逍遥(编号id为9) 所在的班级，显示学生id、姓名、班级名称（连接查询 2表）：


--示例4，查询 软件1班 学习的课程有哪几门，显示班级id、课程名称、课程学分（连接查询）：





--示例5：查询学生的成绩信息：学号，姓名，课程编号，成绩字段
select * from Student
select * from Score
--连接学生信息和成绩信息两张表，连接条件是两表的学号字段

select Student.StudentId,StudentName,CourseId,Score from Student
inner join Score on Student.StudentId=Score.StudentId


--示例6：查询学生的成绩信息：学号，姓名，课程名称，成绩字段
select Student.StudentId,StudentName,CourseName,Score from Student
inner join Score on Score.StudentId = Student.StudentId   --score中间表,StudentId关联学生信息表中学号字段，
inner join Course on Course.CourseId = Score.CourseId     --score中间表,CourseId关联课程信息表中的课程编号



--示例7，查询 李逍遥 学习的课程有哪几门，显示学生id、姓名、课程名称、课程学分（连接查询）：








