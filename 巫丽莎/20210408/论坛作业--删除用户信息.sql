--查询用户信息表中逍遥用户的信息
delete from bbsUsers  where UID=2   --逍遥的用户ID=2

--查询版块表中逍遥创建的版块
delete from bbsSection where sUid=2  --SID=2或4，逍遥创建的板块

--查询主贴表的所有数据
delete from bbsTopic where tSID=2 or tSID=4 or tUID=2  --tid=1或者4

--查询回帖表的所有数据
delete from bbsReply  where rTID=1 or rTID=4 or rUID=2



--修改逍遥用户的是否停用字段
update bbsUsers set uFlag = 1 where UID=2