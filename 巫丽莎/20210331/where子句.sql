--where 条件表达式
--条件表达式包含以下几种运算符：

--1.比较运算符：=，<,<=,>,>=,<>,!=
--示例1，查询软件一班所有的学生：
select * from Student where ClassId=1

--查询班级信息获取班级id
select * from Class

--示例2，查询不是软件一班所有的学生：
select * from Student where ClassId<>1


--2.逻辑运算符：多个条件的练级欸
--				与and 或or 非not
--示例4，查询2000年出生的所有学生：出生日期大于等于2000-01-01 并且 小于2001-01-01
select * from Student where StudentBirth>='2000-01-01' and  StudentBirth<'2001-01-01'


--3.范围运算符：between  a and b  判断字段值在范围[a,b]闭区间之内
--				not between  a and b  判断字段值不在范围[a,b]闭区间之内
--示例5，查询2000年出生的所有学生：出生日期大于等于2000-01-01 并且 小于2001-01-01(用between实现)
select * from Student where StudentBirth between  '2000-01-01' and  '2000-12-31'

--查询成绩信息表中成绩在80-90分的信息
select * from Score where Score between 80 and 90


--查询成绩信息表中成绩不在80-90分的信息
select * from Score where Score not between 80 and 90

--4.空值运算符：is null 判断字符是否为空，为空返回到结果集
--				is not null 判断字符是否不为空，不为空返回到结果集

--示例6，查询生日没有录入的学生：
select *,studentname+StudentAddress from Student where StudentBirth is NULL


--示例6，查询生日不为空的学生：
select * from Student where StudentBirth is not NULL

--示例7，查询家庭住址不为空的学生：
select * from Student where StudentAddress is not null