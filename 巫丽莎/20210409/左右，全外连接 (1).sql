
----------------------------------
--连接查询
--1.内连接:inner join 
--2.外部连接(左，右，全外):left/right/full outer join
----------------------------------


-------------------------------------------------------
--1.内连接：内连接仅仅返回存在字段匹配的记录,即符合连接条件的数据，无法满足的数据被过滤。
--------------------------------------------------------
--查询学生的班级信息：班级编号和班级名称，学号，姓名

select ClassInfo.ClassId 班级编号,ClassName 班级名称,StuId 学号,StuName 姓名 from StuInfo 
inner join ClassInfo on StuInfo.ClassId=ClassInfo.ClassId

--查询学生的成绩信息：学号，姓名，课程编号，成绩

select StuInfo.StuId 学号,StuName 姓名,CourseInfo.CourseId 课程编号,Score 成绩 from StuInfo 
inner join Scores on StuInfo.StuId=Scores.StuId 
inner join CourseInfo on Scores.CourseId=CourseInfo.CourseId

--查询学生选修的课程信息：学号，姓名，选修课程名称

select StuInfo.StuId 学号,StuName 姓名,CourseName 选修课程 from StuInfo 
inner join Scores on StuInfo.StuId=Scores.StuId 
inner join CourseInfo on Scores.CourseId=CourseInfo.CourseId


--查询学生的成绩信息：学号，姓名，课程名称，成绩

select StuInfo.StuId 学号,StuName 姓名,CourseName 课程名称,Score 成绩 from StuInfo 
inner join Scores on StuInfo.StuId=Scores.StuId 
inner join CourseInfo on Scores.CourseId=CourseInfo.CourseId

--查询选修了‘计算机基础’的学生信息：学号，姓名，课程名称，成绩

select StuInfo.StuId 学号,StuName 姓名,CourseName 课程名称,Score 成绩 from StuInfo 
inner join Scores on StuInfo.StuId=Scores.StuId 
inner join CourseInfo on Scores.CourseId=CourseInfo.CourseId 
where CourseName='计算机基础'

-------------------------------------------------------------------------------------------------
--2.外连接：外部连接参与连接的表有主从之分，以主表的每行记录去匹配从表的记录，
--			符合连接条件的数据将直接返回结果集中，不符合连接条件的列将被填充NULL值后再返回结果集中。
--2.外部连接(左，右，全外):left/right/full outer join
-------------------------------------------------------------------------------------------------
--查询所有班级的学生信息：班级编号和班级名称，学号，姓名（有些班级可能没有学生）

select ClassInfo.ClassId 班级编号,StuId 学号,StuName 姓名 from StuInfo 
left join ClassInfo on StuInfo.ClassId=ClassInfo.ClassId

--查询所有班级的学生人数：班级名称，人数(没有学生的班级人数显示为0)
--左查询
select ClassInfo.ClassName 班级名称,count(StuId) 人数 from ClassInfo 
left join StuInfo on StuInfo.ClassId=ClassInfo.ClassId
group by ClassInfo.ClassName

--右查询
select ClassInfo.ClassName 班级名称,count(StuId) 人数 from StuInfo
right join  ClassInfo on StuInfo.ClassId=ClassInfo.ClassId
group by ClassInfo.ClassName

--查询所有班级的男女生人数：班级名称，性别，人数(没有学生的班级人数显示为0)

select ClassName 班级名称,StuInfo.StuSex 性别,count(StuInfo.StuSex) 人数 from ClassInfo 
left join StuInfo on StuInfo.ClassId=ClassInfo.ClassId
group by ClassInfo.ClassName,StuInfo.StuSex
order by ClassName asc

--查询所有学生的成绩信息：学号，姓名，课程编号，成绩（有些学生没有成绩）

select StuInfo.StuId 学号,StuName 姓名,CourseInfo.CourseId 课程编号,Score 成绩 from Scores 
left join StuInfo on Scores.StuId=StuInfo.StuId 
left join CourseInfo on Scores.CourseId=CourseInfo.CourseId

--查询所有学生的学号、姓名、选课总数、所有课程的总成绩,并按照总成绩的降序排列(没成绩的显示为 null )

select StuInfo.StuId 学号,StuName 姓名,count(CourseInfo.CourseId) 选课总数,sum(Score) 总成绩 from CourseInfo 
left join Scores on CourseInfo.CourseId=Scores.CourseId 
left join StuInfo on Scores.StuId=StuInfo.StuId
group by StuInfo.StuId,StuName


--查询所有课程的课程编号、课程名称、选修的学生数量，并按照学生数量的降序排列（没有成绩信息的课程，学生数为0）

select CourseInfo.CourseId 课程编号,CourseName 课程名称,count(CourseName) 选修的学生数量 from CourseInfo 
left join Scores on CourseInfo.CourseId=Scores.CourseId
left join StuInfo on Scores.StuId=StuInfo.StuId
group by CourseInfo.CourseName,CourseInfo.CourseId
order by count(CourseName) desc



--班级表
select * from ClassInfo
--课程表
select * from CourseInfo
--成绩表
select * from Scores
--信息表
select * from StuInfo