create database Student;
use Student;

create table Student (
    StudentId int primary key identity(1,1) not null,
    StudentName nvarchar(50) not null,
    StudentSex tinyint not null  default '3',
    StudentBirth date,
    StudentAddress nvarchar(255) not null default'',
    ClassId int not null
);
select * from Student;


