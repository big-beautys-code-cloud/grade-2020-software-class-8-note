create database ClassCourseId;
alter database ClassCourseId modify name=Course;
create table ClassCourseId(
	CourseId int  primary key identity (1,1) not null,
	CourseName	nvarchar(50) not null,
	CourseCredit	tinyint not null  default '0'
);
 
 select * from ClassCourseId;